#Udp
import socket
#Hash
import hashlib
#Loop Timeout
import time
#Json TimeStamp
import datetime
#Json Dump
import json

# Boat Sender

#If you change this , change it in the server too
secret = "chavalito"

#IP adress (Local NodeJs)
UDP_IP = "127.0.0.1"
#Port
UDP_PORT = 63577

#Actual Json
MESSAGE =  {}
MESSAGE['id'] = 'BOO'                #JSON id

MESSAGE['type'] = 'boat'             #Entity Type
MESSAGE['bearing'] = 0               #Entity Bearing 
MESSAGE['coordinates'] = []          #Entity Coordinates Object Declaration
MESSAGE['antenna'] = []              #Antenna Object Declaration 
MESSAGE['coordinates'].append({      #Cordinates Definition
    'latitude':'0',                  #-Latitude
    'longitude':'0'                  #-Longitude
    })
MESSAGE['antenna'].append({          #Antenna Objet Definition   
    '_id':'DISH1',
    'bearing':'0',
    'tilt':'15'

    })

#JSON timestamp
MESSAGE['timestamp'] = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")

#In order to see what you are sending
print("message:", MESSAGE)

#Hasing Funtion
def hash_message(to_hash):
    sha_signature = hashlib.sha256((to_hash+secret).encode()).hexdigest()
    return sha_signature

#UDP Socket Definition
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP

#Rotation Loop Variable 
rot  = 0
#Bearing Loop Variable
bear = 0
#Latitude Loop Variable
lat  = 61.484686
#Longuitude Loop Variable
lon  = 23.728750
#Antenna Bearing Loop Variable (Aligned By Default)
bear2 = -160.52366074423122
while True:
    #if it reaches +-360 then it takes the value of 0
    if (bear2 >= 360) or (bear2 <= -360):
        bear2 = 0
    #rot    += 4
    #bear  += 3
    #lat   += 0.0004
    #lon   +=
    #bear2 -= 1
    MESSAGE["bearing"] = bear
    MESSAGE["coordinates"][0]['latitude'] = lat
    MESSAGE["coordinates"][0]['longitude'] = lon
    MESSAGE["antenna"][0]['bearing'] = bear2
    #Formating Timestamp
    MESSAGE["timestamp"] = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
    print("message:", MESSAGE)
    #Sending the JSON
    sock.sendto(bytes(str(json.dumps(MESSAGE))+hash_message(str(json.dumps(MESSAGE))),"utf-8"),(UDP_IP, UDP_PORT))
    #Shipping Frequency
    time.sleep(0.8)