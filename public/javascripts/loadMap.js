
/**************************************************************/
/*   Load the whole map and the entities inside               */
/**************************************************************/
/*--- 26 / 03 / 2019 ---*/
/************************/


function initMap() {

    var center;

    /* Last Udp Packet Identier (boat)*/
    var lastBoatAjaxHash="";
    var lastUdpBoat;

    /* Last Udp Packet Identier (drone)*/
    var lastDroneAjaxHash = "";
    var lastUdpDrone;
    


    /* IMPORTANT */
    /* Seems to be working only in 0.1Km lenght antenna for some reason */
    /* It doesnt matter , it's just for calculate antenna vectors       s*/

    /* Boat Antenna Lenght */
    var boatAntennaLenght  = 1;//km 100m
    /* Drone Antenna Lenght*/
    var droneAntennaLenght = 1;//km 100m

    /* Earth Radius Constant in Km*/
    const EARTH_RADIUS = Math.pow(6.371,3);

    /* Coordinates To Load */
    var lake = 
    { 
      lat: 61.484686, /* Latittude */
      lng: 23.728750 /* Longitude */
  };


  /*********************************/
  /* Icons For Google Maps Markers */
  /*********************************/
  /* path : svg language for points  */
  /*  rotation : marker rotation     */
  /*   scale : size of the svg       */       
  /***********************************/
  /* Boat Icon For Google Maps Marker */
  var iconBoat = 
  {
    path:"",
    rotation:0,
    scale:0.9,
}
/* Boat Antenna Icon For Google Maps Marker*/
var iconBoatAntenna = 
{
    path:"",
    rotation:0,
    scale:0.9,
    strokeWeight:1.5
}

/* Drone Icon For Google Maps Marker */
var iconDrone =
{
    path: "M0 0 H 25 V 25 H 0 z",
    rotation: 0,
    scale: 0.6,
    strokeWeight: 1.5
}

/* Drone Antenna Icon For Google Maps Marker */
var iconDroneAntenna = 
{
    path:"M0 0 H30",
    rotation:0,
    scale:0.9,
    strokeWeight:1.5
}


/*Google Maps JavaScript API Object*/
/*Style Modified  : Black and White*/


var map = new google.maps.Map(
   document.getElementById('map'), {
       zoom: 15,//- amount of zoom 
       center: lake,
       styles: [{
           "elementType": "geometry",
           "stylers": [{
               "color": "#f5f5f5"
           }]
       },
       {
           "elementType": "labels.icon",
           "stylers": [{
               "visibility": "off"
           }]
       },
       {
           "elementType": "labels.text.fill",
           "stylers": [{
            "color": "#616161"
        }]
    },
    {
       "elementType": "labels.text.stroke",
       "stylers": [{
        "color": "#f5f5f5"
    }]
},
{
   "featureType": "administrative.land_parcel",
   "elementType": "labels.text.fill",
   "stylers": [{
     "color": "#bdbdbd"
 }]
},
{
   "featureType": "poi",
   "elementType": "geometry",
   "stylers": [{
     "color": "#eeeeee"
 }]
},
{
   "featureType": "poi",
   "elementType": "labels.text.fill",
   "stylers": [{
     "color": "#757575"
 }]
},
{
   "featureType": "poi.park",
   "elementType": "geometry",
   "stylers": [{
     "color": "#e5e5e5"
 }]
},
{
   "featureType": "poi.park",
   "elementType": "labels.text.fill",
   "stylers": [{
     "color": "#9e9e9e"
 }]
},
{
   "featureType": "road",
   "elementType": "geometry",
   "stylers": [{
     "color": "#ffffff"
 }]
},
{
   "featureType": "road.arterial",
   "elementType": "labels.text.fill",
   "stylers": [{
     "color": "#757575"
 }]
},
{
   "featureType": "road.highway",
   "elementType": "geometry",
   "stylers": [{
     "color": "#dadada"
 }]
},
{
   "featureType": "road.highway",
   "elementType": "labels.text.fill",
   "stylers": [{
     "color": "#616161"
 }]
},
{
   "featureType": "road.local",
   "elementType": "labels.text.fill",
   "stylers": [{
      "color": "#9e9e9e"
  }]
},
{
   "featureType": "transit.line",
   "elementType": "geometry",
   "stylers": [{
     "color": "#e5e5e5"
 }]
},
{
   "featureType": "transit.station",
   "elementType": "geometry",
   "stylers": [{
     "color": "#eeeeee"
 }]
},
{
   "featureType": "water",
   "elementType": "geometry",
   "stylers": [{
     "color": "#c9c9c9"
 }]
},
{
   "featureType": "water",
   "elementType": "labels.text.fill",
   "stylers": [{
     "color": "#9e9e9e"
 }]
}]
});


/*********************/
/* Boat Propierties  */
/*********************/
/* Boat  Coordinates */
var boatLatitude  = 61.485827;
var boatLongitude = 23.726741;
/* Boat Coordinates To Update*/
var boatLatitudeToUpdate = boatLatitude;
var boatLongitudeToUpdate = boatLongitude;

/* Boat Rotation */
var boatRotation = 0;
/* Boat Rotation To Update*/
var boatRotationToUpdate = boatRotation;

/* Boat Antenna Coordinates */
var boatAntennaLatitude  = boatLatitude;
var boatAntennaLongitude  = boatLongitude;
/* Boat Antenna Coordinates To Update*/
var boatAntennaLatitudeToUpdate  = boatAntennaLatitude;
var boatAntennaLongitudeToUpdate  = boatAntennaLongitude;

/* Boat Antenna Rotation */
var boatAntennaRotation = 0;
/* Boat Antenna Tilt */
var boatAntennaTilt = 0;

/* Boat Antenna Rotation To Update */
var boatAntennaRotationToUpdate = boatAntennaRotation+1;


/* Boat Antenna Tilt */
var boatAntennaTilt = 90;
/* Boat Antenna Tilt To Update */
var boatAntennaTiltToUpdate = boatAntennaTilt;

/* Boat Svg Points (Exterior) */
var point1BoatExt = [0,-35];
var point2BoatExt = [-15,25];    /* Bottom    - Letf     */
var point3BoatExt = [-13,-20];  /*  Top - Left    */
var point4BoatExt = [13,-20];  /*   Top - Right  */
var point5BoatExt = [15,25];  /*    Bottom    - Right */

/* Boat Svg Points (Interior)*/

var point1BoatInt = [-6,10];    /* Top    - Letf     */
var point2BoatInt = [-6,-10];  /*  Bottom - Left    */
var point3BoatInt = [6,-10];  /*   Bottom - Right  */
var point4BoatInt = [6,10];  /*    Top    - Right */


/* Boat Antenna Svg Point*/
var point1Antenna = [0,35];




/*********************/
/* Drone Properties  */
/*********************/
/* Drone Coordinates */

var droneLatitude = 61.487850;

var droneLongitude = 23.7338;
/*Drone Coordinates To Update*/
var droneLatitudeToUpdate = droneLatitude;
var droneLongitudeToUpdate = droneLongitude;

/* Drone Rotation */
var droneRotation = 0;
/* Drone Rotation To Update*/
var droneRotationToUpdate = droneRotation;

/* Drone Antennas Coordinates */
var droneAntennaLatitude = droneLatitude;
var droneAntennaLongitude = droneLongitude;
/* Drone Antenna Coordinates To Update */
var droneAntennaLatitudeToUpdate = droneAntennaLatitude;
var droneAntennaLongitudeToUpdate = droneAntennaLongitude;

/* Drone Antenna Rotation X */
var droneAntennaRotation = 0;
/* Drone Antenna Rotation Y */
var droneAntennaTilt = 0;


/* Drone Antenna Rotation To Update */
var droneAntennaRotationToUpdate = droneAntennaRotation+1;


/* Drone Svg Points */
var point1Drone = [-10, 15];    /* Top    - Letf     */
var point2Drone = [-10,-15];   /*  Bottom - Left    */
var point3Drone = [ 10,-15];  /*   Bottom - Right  */
var point4Drone = [ 10, 15]; /*    Top    - Right */

/* Drone Antenna Svg Points */
var point1AntennaDrone = [0,35];




/*********************/
/* Cabin Properties  */
/*********************/
/* Cabin Coordinates */
var cabinLatitude = 61.492827;
var cabinLongitude= 23.733741;
/* Cabin Coordinates To Update */
var cabinLatitudeToUpdate = cabinLatitude;
var cabinLongitudeToUpdate = cabinLongitude;


/***********************/
/* Google Maps Markers */
/***********************/


var boatMarker = new google.maps.Marker({
  position: new google.maps.LatLng(boatLatitude,boatLongitude),
  map: map,
  label:{text:'texto'}
});


var droneMarker = new google.maps.Marker({
 position: new google.maps.LatLng(droneLatitude,droneLongitude),
 map: map,
});

var boatAntennaMarker = new google.maps.Marker({
 position: new google.maps.LatLng(boatAntennaLatitude,boatAntennaLongitude),
 map:map,
});

var droneAntennaMarker = new google.maps.Marker({
 position: new google.maps.LatLng(droneAntennaLatitude,droneAntennaLongitude),
 map:map,

});

var straightLine = new google.maps.Polyline({});

var drawDrone = false;
var drawBoat = false;
(function loop() {
  setTimeout(function () {

    /* Path Reset */
    boatMarker.setMap(null);
    boatAntennaMarker.setMap(null);
    droneMarker.setMap(null);
    droneAntennaMarker.setMap(null);
    straightLine.setMap(null);

    /*  AJAX Request */
    const request = new XMLHttpRequest();

    /* AJAX Request Vars*/
    var ajaxJson;
    var ajaxHash;

    /* Flag Change Hash --> Next Boat/ Drone Position */
    var flagReplaceBoatDrone;

    /* Cartisian Point, Boat */
    var boatPointCartisianPlan = parseFromPolarToCartesian(boatLatitudeToUpdate,boatLongitudeToUpdate);
    /* Cartisian Point, Drone */
    var dronePointCartisianPlan = parseFromPolarToCartesian(droneLatitudeToUpdate,droneLongitudeToUpdate);

    /* Cartisian Point, End Point Antenna */
    var antennaBoatEnd  = Math.pointFromDistanceAngleAndPoint(boatAntennaLenght,boatAntennaRotationToUpdate,boatPointCartisianPlan);
    /* Cartisian Point, End Point Drone Antenna*/
    var antennaDroneEnd = Math.pointFromDistanceAngleAndPoint(droneAntennaLenght,droneAntennaRotationToUpdate,boatPointCartisianPlan);

    /* Boat Antenna Tilt */
    var boatAntennaTilt = 20; //Degrees
    /* Drone Antenna Tilt*/
    var droneAntennaTilt = 30; //Degrees


    boatAntennaRotationToUpdate = boatAntennaRotationToUpdate + 1;

    /* Center Of the Svg (Boat) */
    boatCenter  = centerOfRectagle(point2BoatExt,point4BoatExt);
    /* Center Of Svg (Drone) */
    droneCenter = centerOfRectagle(point1Drone  ,point3Drone  );

    /* Vector that the antennas should match*/
    var vectorToMatch     = Math.vectorFrom(boatPointCartisianPlan,dronePointCartisianPlan);

    /* Vector de Barco-Antena*/
    var boatAntennaVector = Math.vectorFrom(boatPointCartisianPlan,antennaBoatEnd);

    /* OTHER INFO */

    /* Distance In Km */
    var distanceBetweenAtennas = getDistanceBetweenPolarPoints([boatLatitudeToUpdate,boatLongitudeToUpdate],
        [droneLatitudeToUpdate,droneLongitudeToUpdate]);

    
    var radiansAntennaBoat = Math.acos( (Math.pow(distanceBetweenAtennas,2) + Math.pow(EARTH_RADIUS,2) - Math.pow(EARTH_RADIUS,2)) 
     / (2 * distanceBetweenAtennas * EARTH_RADIUS));

    /* Angle made by the antenna and the center of earth */
    var angleAntennaBoat = Math.degrees(angleAntennaBoat);
    /* Antennas So close that wouldnt make any diference*/ //it will make difference with altitude
    var radiansAntennaDrone = angleAntennaBoat;

    /* Pretty Expanatory itself*/ /* Also , it's Tilt*/
    var alignedAntennaTilt = Math.acos( (Math.pow(EARTH_RADIUS,2) + Math.pow(EARTH_RADIUS,2) - Math.pow(distanceBetweenAtennas,2)) 
     / (2 * distanceBetweenAtennas * EARTH_RADIUS));

    /* Bearing Between The Boat and The Drone*/
    let bearingBetweenAntennas = getBearingBetween([droneLatitudeToUpdate,droneLongitudeToUpdate],[boatLatitudeToUpdate,boatLongitudeToUpdate]);
    let bearingBetweenAntennas2 = getBearingBetween([boatLatitudeToUpdate,boatLongitudeToUpdate],[droneLatitudeToUpdate,droneLongitudeToUpdate]);

    var angleBetweenVector = Math.vectorsAngle(vectorToMatch,boatAntennaVector);

    console.log(bearingBetweenAntennas);
    console.log(bearingBetweenAntennas2);

    /*-------------------*/
    /*        AJAX       */
    /*-------------------*/

    let antennaAlignmentLine = false;

    /*Boat Ajax*/
    let boatUrl = '/api/getBoatUdpPacket';

    boatAjax = (url) => 
    {

        let  xhr =  new XMLHttpRequest();
        let hashLenght = 64;

        xhr.onreadystatechange = () =>{
            if(xhr.readyState === 4){
                let ajaxResponse = xhr.response;
                let actualHash = ajaxResponse.substr(ajaxResponse.length - 64);

                if(xhr.response == undefined || xhr.response == "")
                {
                    drawBoat = false;
                }
                else
                {
                    drawBoat = true;
                    lastBoatAjaxHash = actualHash;
                    ajaxResponse = JSON.parse(ajaxResponse);
                    lastUdpBoat = ajaxResponse;

                    /*  It has The udp Packet but there is a posibility that the other one is undefined */
                }
                /* If packet not null neither undefined */
                if(drawBoat){

                    /*-------------*/
                    /*  BOAT PATH  */
                    /*-------------*/

                    boatMarker.setMap(null);
                    boatMarker = new google.maps.Marker({
                        position: new google.maps.LatLng(ajaxResponse.coordinates[0].latitude,ajaxResponse.coordinates[0].longitude),
                        map: map,
                        icon:{
                            path:
                            " M "+boatCenter[0]   +" "+boatCenter[1]   + 
                            " L "+point2BoatExt[0]+" "+point2BoatExt[1]+ 
                            " L "+point3BoatExt[0]+" "+point3BoatExt[1]+ 
                            " L "+point1BoatExt[0]+" "+point1BoatExt[1]+ 
                            " L "+point4BoatExt[0]+" "+point4BoatExt[1]+ 
                            " L "+point5BoatExt[0]+" "+point5BoatExt[1]+
                            " L "+point2BoatExt[0]+" "+point2BoatExt[1]+
                            " M "+point1BoatInt[0]+" "+point1BoatInt[1]+
                            " L "+point2BoatInt[0]+" "+point2BoatInt[1]+
                            " L "+point3BoatInt[0]+" "+point3BoatInt[1]+
                            " L "+point4BoatInt[0]+" "+point4BoatInt[1]+" z"
                            , 
                            rotation:ajaxResponse.bearing,
                            scale:0.9,
                        }
                    });

                    /*------------------*/
                    /* BOAT ANTENNA PATH*/
                    /*------------------*/

                    boatAntennaMarker.setMap(null);
                    boatAntennaMarker = new google.maps.Marker({
                        position: new google.maps.LatLng(ajaxResponse.coordinates[0].latitude,ajaxResponse.coordinates[0].longitude),
                        map:map,
                        icon: 
                        {
                            path:
                            " M "+boatCenter[0]   +" "+boatCenter[1]   +
                            " L "+point1Antenna[0]+" "+point1Antenna[1],
                            rotation:ajaxResponse.antenna[0].bearing,
                            scale:0.9
                        }
                    });

                    boatLatitudeToUpdate = ajaxResponse.coordinates[0].latitude;
                    boatLongitudeToUpdate = ajaxResponse.coordinates[0].longitude;

                    /* Let's check if the antennas are aligned ( boat ) */
                    if( (lastUdpDrone != undefined || lastUdpDrone != "")
                     &&
                     (lastUdpDrone.antenna[0].bearing != "" || 
                      lastUdpDrone.antenna[0].bearing != undefined)){
                        /* Check if the degrees match with error range of 2 +- (boat) */
                    if( (lastUdpBoat.antenna[0].bearing >= bearingBetweenAntennas - 2 &&
                        lastUdpBoat.antenna[0].bearing <= bearingBetweenAntennas + 2) &&
                        (lastUdpDrone.antenna[0].bearing >= bearingBetweenAntennas2 -2 &&
                          lastUdpDrone.antenna[0].bearing <= bearingBetweenAntennas2 + 2)){

                        /* Coordinates of the straight line (2 points)*/
                    let pathCoordinates = 
                    [
                    /*  Boat Coordinates  */
                    {lat:lastUdpBoat.coordinates[0].latitude,lng:lastUdpBoat.coordinates[0].longitude},
                    /*  Drone Coordinates */
                    {lat:lastUdpDrone.coordinates[0].latitude,lng:lastUdpDrone.coordinates[0].longitude}
                    ]

                    straightLine = new google.maps.Polyline({
                        path:pathCoordinates,
                        geodesic:true,
                        strokeColor:'#53f442'
                    });

                    straightLine.setMap(map);
                }
            }else{
                console.log('not aligned or missind packet');
            }
        }else{
            boatMarker.setMap(null);
            boatMarker = new google.maps.Marker({
              position: new google.maps.LatLng(boatLatitudeToUpdate,boatLongitudeToUpdate),
              map: map,
              label:{text:'LAST POSITION KNOWN'}
          });
            boatAntennaMarker.setMap(null);
            straightLine.setMap(null);
        }
    }
}
xhr.open('GET',url,true);
xhr.send();
}  
boatAjax(boatUrl);

let droneUrl = '/api/getDroneUdpPacket';

/* Drone Ajax */
droneAjax = (url) =>
{

    let xhr = new XMLHttpRequest();
    let hashLenght = 64;

    xhr.onreadystatechange = () =>
    {
        if(xhr.readyState === 4){
            let ajaxResponse = xhr.response;
            let actualHash = ajaxResponse.substr(ajaxResponse.lenght - 64);

            if(xhr.response == undefined || xhr.response == ""){
                drawDrone  = false
                console.log(xhr.response);
            }
            else
            {
                drawDrone = true;
                lastDroneAjaxHash = actualHash;
                ajaxResponse = JSON.parse(ajaxResponse);
                lastUdpDrone = ajaxResponse;
            }
            /* If packet not null neither undefined */
            if(drawDrone){

             /*------------*/
             /* DRONE PATH */
             /*------------*/

             droneMarker.setMap(null);

             droneMarker = new google.maps.Marker({
                 position: new google.maps.LatLng(ajaxResponse.coordinates[0].latitude,ajaxResponse.coordinates[0].longitude),
                 map: map,
                 icon: {
                    path:" M "+droneCenter[0]+" "+droneCenter[1]+
                    " L "+point1Drone[0]+" "+point1Drone[1]+
                    " L "+point2Drone[0]+" "+point2Drone[1]+
                    " L "+point3Drone[0]+" "+point3Drone[1]+
                    " L "+point4Drone[0]+" "+point4Drone[1]+
                    " L "+point1Drone[0]+" "+point1Drone[1]
                    ,
                    rotation:ajaxResponse.bearing,
                    scale:0.9
                }
            });

             /*-------------*/
             /*DRONE ANTENNA*/
             /*-------------*/

             droneAntennaMarker.setMap(null);
             droneAntennaMarker = new google.maps.Marker({
                position: new google.maps.LatLng(ajaxResponse.coordinates[0].latitude,ajaxResponse.coordinates[0].longitude),
                map:map,
                icon: {
                    path:
                    " M "+droneCenter[0]   +" "+droneCenter[1]   +
                    " L "+point1AntennaDrone[0]+" "+point1AntennaDrone[1],
                    rotation:ajaxResponse.antenna[0].bearing,
                    scale:0.9
                }
            });
             lastAjaxPacket = "";

             droneLatitudeToUpdate  = ajaxResponse.coordinates[0].latitude;
             droneLongitudeToUpdate = ajaxResponse.coordinates[0].longitude;

             /*-------------------------------------------------*/
             /* Could be necessary to control alignment here too*/
             /*-------------------------------------------------*/

             /* Let's check if antennas are aligned ( drone ) *///dead code : could be usefull in future
             if( (lastUdpBoat != undefined || lastUdpBoat != "")
                 &&
                 (lastUdpBoat.antenna[0].bearing != "" || 
                  lastUdpBoat.antenna[0].bearing != undefined)){
                /* Check if the degrees match with error range of 2 +- (boat) */
            if(lastUdpDrone.antenna[0].bearing >= bearingBetweenAntennas - 2 &&
                lastUdpDrone.antenna[0].bearing <= bearingBetweenAntennas + 2){
             /* Coordinates of the straight line (2 points)*/
         let pathCoordinates = 
         [
         /*  Boat Coordinates  */
         {lat:lastUdpBoat.coordinates[0].latitude,lng:lastUdpBoat.coordinates[0].longitude},
         /*  Drone Coordinates */
         {lat:lastUdpDrone.coordinates[0].latitude,lng:lastUdpDrone.coordinates[0].longitude}
         ]
     }
 }
}
else
{
    droneMarker.setMap(null);
    droneMarker = new google.maps.Marker({
     position: new google.maps.LatLng(droneLatitudeToUpdate,droneLongitudeToUpdate),
     label:{text:"LAST KNOWN POSITION"},
     map: map,
 });

    droneAntennaMarker.setMap(null);
}
}
}
xhr.open('GET',url,true);
xhr.send();
}
droneAjax(droneUrl);



loop();
}, 2000);
}());


/* Rotates a Vector (You have to call the method 2 times (1 per point ))*/
function rotateVector (x1,y1,rotation) {

    let newx1;
    let newy1;

    newx1 = x1 * Math.cos(rotation) + y1 * Math.sin(rotation);
    newy1 = x1 * -Math.sin(rotation) + y1 * Math.cos(rotation);

    return [newx1,newy1];
}


function centerOfRectagle (point1,point2) {
    return [(point1[0] + point2[0])/2,(point1[1] + point2[1])/2]
}


/*  Status : NOK */
/* Should check if the antennas are aligned */
function rotationAlignment (angle1,angle2) {
  /* Body...*/      
}

/********************/
/* [0] Latitude  x2 */
/*  [1] Longitude x2*/
/*------------------*//*-----*/
/* Params already in radians */
/*  Result in Degrees        *//*------*/
/* Gets the bearing between two points */
/*----------------------------/*-------*/
/*    Error range = 5º ><    */
    function getBearingBetween (point1,point2)  //
    {
        /* Latitudes of 2 Points*/
        let lat1 = point1[0];
        let lat2 = point2[0];

        /* Logitude of 2 Points*/
        let lng1 = point1[1];
        let lng2 = point2[1];

        let yVar = Math.sin( lng2 - lng1 ) * Math.cos( lat2 );
        let xVar = Math.cos( lat1 ) * Math.sin( lat2 ) - 
        Math.sin( lat1 ) * Math.cos( lat2 ) *
        Math.cos( lng2 - lng1 ); 

        return Math.degrees(Math.atan2(yVar, xVar));
    }

    /*********************/
    /* [0] Latitude  x2  */
    /*  [1] Longitude x2 */
    /*********************/
    /* Params already in radians */
    /*  Result   in   Km         */
    /* Calcs the distance in EARTH_RADIUS measure between 2 points in the coordinates */
    function getDistanceBetweenPolarPoints (point1,point2) 
    {
        /* Earth Radius */
        let R = EARTH_RADIUS;

        /* Latitudes of 2 Points*/
        let lat1 = point1[0];
        let lat2 = point2[0];

        /* Logitude of 2 Points*/
        let lng1 = point1[1];
        let lng2 = point2[1];

        /* Latitude and Longitude Differences */
        let latDifference = lat1 - lat2;
        let lngDifference = lng1 - lng2; 

        let aVar = (Math.sin(latDifference/2)) * (Math.sin(latDifference/2)) +
        (Math.cos(lat1)) * (Math.cos(lat2)) * Math.sin(lngDifference) *
        (Math.sin(lngDifference));



        let cVar = 2 * Math.atan2( Math.sqrt(aVar) , Math.sqrt(1-aVar) );

        /* d = Distance */
        return  R * cVar;
    }


    /*  Status : NOK                  */ 
    /**********************************/
    /* [0] Bearing                    */
    /**********************************/
    /* Parameter should be in degrees */
    /*It takes the bearing an calculates the angle wich should the antenna have*/
    function getAngleByBearingAndAngle (bearing,angle) {
        return -((angle) + bearing) - 180;
    }

    /* Get Point from Cartesian Plan from Distance and angle to it*/
    function getPointFromDistanceAndAngle (distance,angle,poin1Cartessian) {
        return  [ (distance * Math.cos(angle) + point1[0]) 
        , (distance * Math.sin(angle) + point1[1]) ];
    }

    /*Calculate /B Angle With Sides /b and /a And Angle /A*/
    function getAngleFromSidesAndAnotherAngle (angleB,sideB,sideA){;
        return Math.acos(sideB * Math.sin(angleB) / sideA)
    }

    /* lat must come */
    /* Both Params Must Came Radians */
    function parseFromPolarToCartesian(latitude,longitude){
        return [(EARTH_RADIUS * Math.radians(latitude) * Math.cos(longitude) )
        ,(EARTH_RADIUS * Math.radians(latitude) * Math.sin(longitude) )];
    }


    /* Status : OK*/
    /* Convert from degrees to radians */
    Math.radians = function(degrees) {
        return (degrees * Math.PI) / 180;
    }
    /* Status : OK*/
    /* Convert from radians to degrees */
    Math.degrees = function(radians) {
        return (radians * 180) / Math.PI;
    }

    /* Re : Km */
    Math.vectorFrom = function(begin,end) {
        return [(end[0]-begin[0]),(end[1]-begin[1])];
    }



    /* Calcs new point from 3 params --> Dsitance Angle and Point */
    Math.pointFromDistanceAngleAndPoint = function (distance,angle,point) {

        /* X [0] and Y [1] Axis Positive*/
       // if( (point[0] > 0) && (point[1] > 0) )
        //{
            return [( (distance *  Math.cos(Math.radians(angle))) + point[0] ),   //X 
                    ( (distance *  Math.sin(Math.radians(angle))) + point[1] )]; //Y
       // }
       
       /* X [0] Axis Positive and Y [1] Axis Negative */
       // if( (point[0] > 0) && (point[1] < 0) )
       // { 
       //     return [( (distance *   Math.cos(angle)) + point[0] ), //X
        //            ( (distance *  -Math.sin(angle)) + point[1] )];//Y
       // }

       /* X [0] Axis Negative and Y [1] Axis Negative*/
       // if( (point[0] < 0) && (point[1] < 0) )
       // {
        //    return [( (distance * -Math.cos(angle)) + point[0] ),//X
       //             ( (distance * -Math.sin(angle)) + point[1] )];//Y
       // }

       /* X [0] Axis Negative and Y [1] Axis Positive*/
        //if( (point[0] < 0 ) && (point[1] > 0) )
       // {
       //     return [(( distance *  -Math.cos(angle)) + point[0] ),
        //            (( distance *   Math.sin(angle)) + point[1] )]; 
       // }
   }

   /* Basicly An Array Compare*/
   Math.equalVectors = function(vector1,vector2){
    if (vector1 === vector2) return true;
    if (vector1 == null || vector2 == null) return false;
    if (vector1.length != vector2.length) return false;
    if((Math.roundExactDecimal(vector1[0],8)/Math.roundExactDecimal(vector1[1],8)) == (Math.roundExactDecimal(vector2[0],8)/Math.roundExactDecimal(vector2[1],8))){
        return true;
    }
        // If you don't care about the order of the elements inside
        // the array, you should sort both arrays here.
        // Please note that calling sort on an array will modify that array.
        // you might want to clone your array first.

        for (var i = 0; i < vector1.length; ++i) {
          if (vector1[i] !== vector2[i]) return false;
      }


      return true;
  }

  /* Rounds the parameter to an exact decimal */
  Math.roundExactDecimal = function(numberToRound,decimalToRound){
    let numberStringyfied = numberToRound.toFixed(decimalToRound);
    return parseFloat(numberStringyfied);
}

/* Calcs the angle form from 2 vectors */
Math.vectorsAngle = function(vector1,vector2,flagRadians) {

    return flagRadians ? Math.atan2( (vector2[1] ) - (vector1[1] ),(vector2[0]) - (vector1[0]) ) 
    : Math.atan2( (vector2[1] ) - (vector1[1] ),(vector2[0]) - (vector1[0]) ) * 180 / Math.PI;

}

}

